package main

import (
	"log"
	Server "webserver/server"
)

// option 3 -  Functional Options Pattern (Main)
/*
The functional options pattern allows us to define a fixed type signature for each and any possible configuration of our server, buy using the func(*Server) type signature we can create any option to be passed to the server. Our options are also optional by default, so it’s easy to swap any options without any major problem. This approach is also good given the expressive design and the auto-documenting nature of the type definitions, each method defines the option and the type of option for your server.
*/

func main() {
	svr := Server.New(Server.WithHost("localhost"), Server.WithPort(3000))
	if err := svr.Start(); err != nil {
		log.Fatal(err)
	}
	if err := svr.Start(); err != nil {
		log.Fatal(err)
	}
}
