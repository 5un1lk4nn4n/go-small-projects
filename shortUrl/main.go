package main

import (
	"fmt"
	"net/http"
	"shortUrl/shortner"
)

func main() {

	mux := defaultMux()

	pathToUrls := map[string]string{
		"/usg": "https://godoc.org/github.com/gophercises/urlshort",
		"/ggl": "https://google.com",
	}

	mapHandler := shortner.MapHandler(pathToUrls, mux)

	fmt.Println("Starting the server on : 8081")
	http.ListenAndServe(":8081", mapHandler)

}

func defaultMux() *http.ServeMux {

	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux

}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello!")
}
