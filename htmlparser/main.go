package main

import (
	link "htmlparser/parser"
	"strings"
)

var htmlData = `
<html>
  <head>
    <title>Href Attribute Example</title>
  </head>
  <body>
    <h1>Href Attribute Example</h1>
    <p>
      <a href="https://www.freecodecamp.org/contribute/">The freeCodeCamp Contribution Page</a> shows you how and where you can contribute to freeCodeCamp's community and growth.
    </p>
  </body>
</html>
`

func main() {

	r := strings.NewReader(htmlData)
	l, err := link.Parse(r)
	if err != nil {
		panic(err)
	}
	println(l)

}
