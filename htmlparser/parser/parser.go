package link

import (
	"io"

	"golang.org/x/net/html"
)

type Link struct {
	Href string
	Text string
}

func Parse(r io.Reader) ([]Link, error) {

	d, err := html.Parse(r)
	if err != nil {
		panic(err)
	}
	var ns []*html.Node = linkNodes(d)
	for n := range ns {
		println(n)
	}

	return nil, nil
}

func linkNodes(n *html.Node) []*html.Node {
	if n.Type == html.ElementNode && n.Data == "a" {
		return []*html.Node{n}
	}

	var an []*html.Node
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		an = append(an, linkNodes(c)...)
	}
	return an
}

// func dfs(n *html.Node) {
// 	fmt.Println(n.Data)
// 	for c := n.FirstChild; c != nil; c = c.NextSibling {
// 		dfs(c)
// 	}

// }
