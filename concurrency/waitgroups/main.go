package main

import (
	"fmt"
	"sync"
)

func main() {

	var wg sync.WaitGroup

	words := []string{
		"alpha",
		"beta",
		"delta",
		"gamma",
		"pi",
		"zeta",
		"eta",
		"theta",
		"epsilon",
	}

	wg.Add(len(words))

	for i, w := range words {
		go printWords(fmt.Sprintf("%d:%s", i, w), &wg)
	}
	wg.Wait()

}

func printWords(w string, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(w)

}
