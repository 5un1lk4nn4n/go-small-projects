package main

import (
	"fmt"
	"mutex/income"
	"sync"
)

var msg string
var wg sync.WaitGroup

func updateMessage(s string, m *sync.Mutex) {
	defer wg.Done()
	m.Lock()
	msg = s
	m.Unlock()
}
func main() {
	var mutex sync.Mutex
	msg = "hello world!"
	wg.Add(2)

	go updateMessage("Hello Universe!", &mutex)
	go updateMessage("Hello MultiUniverse!", &mutex)

	wg.Wait()

	fmt.Println(msg)

	income.CalculateIncome()
}
