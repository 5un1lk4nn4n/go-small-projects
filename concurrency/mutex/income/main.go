package income

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

type Income struct {
	Source string
	Amount int
}

func CalculateIncome() {
	var bankBalance int
	var balance sync.Mutex
	fmt.Printf("Initial balance: $%d.00\n", bankBalance)
	incomes := []Income{
		{Source: "Main", Amount: 1000},
		{Source: "Part", Amount: 200},
		{Source: "Investment", Amount: 600},
		{Source: "Gifts", Amount: 50},
	}

	wg.Add(len(incomes))

	for i, income := range incomes {
		go func(i int, income Income) {
			defer wg.Done()
			for week := 1; week < 52; week++ {
				balance.Lock()
				temp := bankBalance
				temp += income.Amount
				bankBalance = temp
				balance.Unlock()
				fmt.Printf("On week %d, you earned $%d.00 from %s\n", week, income.Amount, income.Source)
			}
		}(i, income)
	}
	wg.Wait()
	fmt.Printf("Annual income is $%d.00\n", bankBalance)

}
