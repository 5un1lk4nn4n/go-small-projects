package income

import (
	"io"
	"os"
	"strings"
	"testing"
)

func Test_main(t *testing.T) {
	stdOut := os.Stdout

	r, w, _ := os.Pipe()

	os.Stdout = w

	CalculateIncome()

	_ = w.Close()

	result, _ := io.ReadAll(r)
	o := string(result)
	os.Stdout = stdOut

	if !strings.Contains(o, "$94350.00") {
		t.Error("unexpected result")
	}

}
