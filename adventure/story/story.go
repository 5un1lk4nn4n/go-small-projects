package story

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.New("").Parse(defaultHandlerTmpl))
}

var defaultHandlerTmpl = `<html>
<head>
	<meta charset="utf-8" />
	<title>Choose your own adventure</title>
</head>
<body>
	<h1>{{.Title}}</h1>
	{{range .Paragraphs}}
	<p>{{.}}</p>
	{{end}}
	<ul>
		{{range .Options}}
		<li><a href="/{{.Chapter}}">{{.Text}}</a></li>
		{{end}}
	</ul>
</body>
</html>`

type HandlerOpts func(h *handler)

func WithTemplate(t *template.Template) HandlerOpts {
	return func(h *handler) {
		h.t = t
	}
}
func NewHandler(s Story, opts ...HandlerOpts) http.Handler {
	h := handler{s, tpl}

	for _, opt := range opts {
		if opt != nil {
			opt(&h)
		}
	}
	return h

}

type handler struct {
	s Story
	t *template.Template
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := strings.TrimSpace(r.URL.Path)
	if path == "" || path == "/" {
		path = "/intro"
	}
	path = path[1:]
	if c, ok := h.s[path]; ok {
		err := h.t.Execute(w, c)
		if err != nil {
			log.Printf("%v", err)
			http.Error(w, "Something went wrong...", http.StatusInternalServerError)
		}
		return
	}
	http.Error(w, "Chapter not found", http.StatusNotFound)

}

func JsonStory(r io.Reader) (Story, error) {
	d := json.NewDecoder(r)
	var s Story
	if err := d.Decode(&s); err != nil {
		return nil, err
	}
	return s, nil
}

type Story map[string]chapter

type chapter struct {
	Title      string   `json:title`
	Paragraphs []string `json:paragraphs`
	Options    []Option `json:options`
}

type Option struct {
	Text    string `json:text`
	Chapter string `json:chapter`
}
