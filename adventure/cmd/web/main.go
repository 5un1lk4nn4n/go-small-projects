package main

import (
	"adventure/story"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	port := flag.Int("port", 3000, "Port number")
	filename := flag.String("file", "story.json", "Stories for game")
	flag.Parse()
	// path := filepath.Join("adventure","story",filename)
	f, err := os.Open(*filename)
	if err != nil {
		panic(err)
	}
	s, err := story.JsonStory(f)
	if err != nil {
		panic(err)
	}
	// h := story.NewHandler(s, story.WithTemplate(nil))
	h := story.NewHandler(s)
	fmt.Println("Starting the server on port", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), h))

}
