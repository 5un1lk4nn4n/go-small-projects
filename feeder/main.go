package main

import (
	_ "feeder/matchers"
	"feeder/search"
	"log"
	"os"
)

func init() {
	// Change the device for logging to  stdout
	log.SetOutput(os.Stdout)
}

func main() {
	search.Run("president")
}
