package cmd

import (
	"fmt"
	"strings"
	"taskcli/db"

	"github.com/spf13/cobra"
)

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Adds a task to your list",
	Run: func(cmd *cobra.Command, args []string) {
		task := strings.Join(args, " ")

		_, err := db.CreateTask(task)
		if err != nil {
			fmt.Println("Something went wrong!")
			return
		}

		fmt.Printf("Added %s\n", task)
	},
}

func init() {
	RootCmd.AddCommand(addCmd)
}
