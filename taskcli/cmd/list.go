package cmd

import (
	"fmt"
	"os"
	"taskcli/db"

	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list of active tasks",
	Run: func(cmd *cobra.Command, args []string) {
		tasks, err := db.AllTasks()

		if err != nil {
			fmt.Println("Error!")
			os.Exit(1)
		}
		if len(tasks) == 0 {
			fmt.Println("You have no tasks!")
			return
		}

		fmt.Println("You have following tasks!")
		for i, t := range tasks {
			fmt.Printf("%d. %s\n", i+1, t.Value)
		}

	},
}

func init() {
	RootCmd.AddCommand(listCmd)
}
