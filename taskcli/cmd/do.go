package cmd

import (
	"fmt"
	"strconv"
	"taskcli/db"

	"github.com/spf13/cobra"
)

var doCmd = &cobra.Command{
	Use:   "do",
	Short: "completes the task",
	Run: func(cmd *cobra.Command, args []string) {

		var ids []int
		for _, arg := range args {
			id, err := strconv.Atoi(arg)

			if err != nil {
				fmt.Println("Failed to parse!", arg)
			} else {
				ids = append(ids, id)
			}

		}

		tasks, err := db.AllTasks()

		if err != nil {
			fmt.Println("Something went wrong")
			return
		}
		for _, id := range ids {
			if id < 0 || id > len(tasks) {
				fmt.Printf("%d is not valid id\n", id)
				continue
			}

			task := tasks[id-1]

			err := db.DeleteTask(task.Key)

			if err != nil {
				fmt.Printf("%d is failed to make as complete", id)
			} else {
				fmt.Printf("%d is  marked as complete", id)
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(doCmd)
}
